package com.example;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.lang.StringBuilder;

import com.google.common.io.CharStreams;
import com.google.common.base.Charsets;
import com.google.common.base.Splitter;

public class Dog {
    public String GenDog(int scale) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStream input = getClass().getResourceAsStream("/dog1.txt");
        String result = CharStreams.toString(new InputStreamReader(input, Charsets.UTF_8));
        Iterator<String> iterator = Splitter.on('\n').split(result).iterator();
        boolean first = true;
        while (iterator.hasNext()) {
            String line = iterator.next();
            StringBuilder lsb = new StringBuilder();
            for (int p = 0; p < line.length(); p++) {
                for (int i = 0; i < scale; i++) {
                    lsb.append(line.charAt(p));
                }                    
            }
            if (first) {
                first = false;
            } else {
                sb.append('\n');
            }
            String scaledLine = lsb.toString();
            for (int i = 0; i < scale; i++) {
                if (i > 0) {
                    sb.append('\n');
                }
                sb.append(scaledLine);
            }
        }
        return sb.toString();
    }
}
