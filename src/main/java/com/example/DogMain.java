package com.example;

abstract class DogMain {
    public static void main(String[] args) throws Exception {
        int scale = 1;
        if (args.length > 0) {
            scale = Integer.parseInt(args[0]);
        }
        Dog dog = new Dog();
        System.out.println(dog.GenDog(scale));
    }
}
