#!/bin/bash
set -eu
SCALEMAX=3
for (( i=1; i<=SCALEMAX; i++)); do
    ./docker-run.sh ${i} >"src/test/resources/testdata/dog1-${i}.txt.golden"
done
